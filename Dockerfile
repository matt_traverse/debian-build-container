FROM debian:stretch
RUN echo 'deb-src http://deb.debian.org/debian stretch main' >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y  build-essential debhelper dh-systemd systemd reprepro openssh-client \
	dh-python python-setuptools python-all python-nose libpcap0.8-dev flex bison libdumbnet-dev git \
	libnet1-dev libpcre3-dev libpq-dev libprelude-dev libnfnetlink-dev libnetfilter-queue-dev pkg-config zlib1g-dev \
	texlive texlive-latex-base texlive-font-utils texlive-binaries ghostscript libnetfilter-log-dev python-six golang-go \
	dpatch debconf-utils libmnl-dev libnetfilter-conntrack-dev libnetfilter-conntrack3 libnftnl-dev devscripts bc  \
	time qemu-utils python3-pexpect python3-requests kmod cpio libssl-dev dkms
RUN apt-get install sudo && echo 'build ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/build-dpkg
RUN useradd -m build
USER build
